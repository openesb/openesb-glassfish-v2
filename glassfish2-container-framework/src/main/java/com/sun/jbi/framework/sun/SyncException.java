/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SyncException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.sun;

/**
 * 
 */

public class SyncException 
        extends Exception
{
    /**
     * Creates a new instance of SyncException with an exception 
     * message.
     * 
     * @param aMessage String describing this exception.
     */
    public SyncException (String aMessage)
    {
        super(aMessage);
    }

    /**
     * Creates a new instance of SyncException with the specified 
     * message and cause.
     * @param aMessage String describing this exception.
     * @param aCause Throwable which represents an underlying problem
     * (or null).
     */
    public SyncException (String aMessage, Throwable aCause)
    {
        super(aMessage, aCause);
    }

   /**
     * Creates a new instance of SyncException with the specified cause.
     * @param aCause Throwable which represents an underlying problem
     * (or null).
     */
    public SyncException (Throwable aCause)
    {
        super(aCause);
    }
}
