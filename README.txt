Author : Param
Date : 13-March-2014

Binary : glassfish2-container-core.jar

==================
Description: 
==================
OpenESB Core runtime for Glassfish v2. The jar is having exactly same format as openesb-core.jar having 

	com
	META-INF
	build.xml
	jbi_install.jar
		jbi
			..
			lib
				..
				jbi_rt.jar
				..
			..
			
			
--------------------------------

=================
How to build: 
=================
glassfish2-container$ mvn clean install

....

[INFO] glassfish2-container .............................. SUCCESS [4.532s]
[INFO] glassfish2-container-util ......................... SUCCESS [2.848s]
[INFO] glassfish2-container-bootstrap .................... SUCCESS [5.286s]
[INFO] glassfish2-container-framework .................... SUCCESS [6.125s]
[INFO] openesb-core-installer ............................ SUCCESS [2.755s]
[INFO] glassfish2-installer .............................. SUCCESS [7.027s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 28.784s

....


=====================
Binary file location : 
=====================

glassfish2-container\glassfish2-installer\target\glassfish2-container-core.jar





